module gitlab.com/mergetb/tech/rtnl

require (
	github.com/fatih/color v1.7.0
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mdlayher/netlink v0.0.0-20190221230908-86708d8e0a55
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd // indirect
	golang.org/x/sys v0.0.0-20190222072716-a9d3bda3a223
)
